<?php


namespace tapp;


use app\person;

class student extends person
{
    private $studentID;

    public function setStudentID($studentID)
    {
        $this->studentID = $studentID;
    }

    public function getStudentID()
    {
        return $this->studentID;
    }

}

